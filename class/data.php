<?
class wp_data__class__data extends wp_data__class__data__parent
{
	function __construct()
	{
		parent::{__function__}();
		include_once('tmp/system/library/file/CFile.php');
		$this->cfile = new CFile();
	}
	
	function get_data($d=null)
	{
		$W = $this->C->db()->where_interpreter(array(
			'ID'		=> "id IN ('ID')",
			'SEONAME'	=> "id IN (SELECT file_id FROM data_file_seoname WHERE name LIKE 'SEONAME')",
		),$this->D['MODUL']['D']['wp_data']['DATA']['W']);
		#$L = (strlen($this->D['HELPDESK']['D']['TICKET']['L']['START']) && strlen($this->D['HELPDESK']['D']['TICKET']['L']['STEP']))? " LIMIT {$this->D['HELPDESK']['D']['TICKET']['L']['START']},{$this->D['HELPDESK']['D']['TICKET']['L']['STEP']}":'';
		
		$qry = $this->C->db()->query("SELECT id, active, extension, size, utimestamp,itimestamp FROM data_file WHERE 1 {$W} {$L}");
		while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
		{
			$this->D['MODUL']['D']['wp_data']['DATA']['D'][ $ary['id'] ] = [
				'ACTIVE'		=> $ary['active'],
				'EXTENSION'		=> $ary['extension'],
				'SIZE'			=> $ary['size'],
				'UTIMESTAMP'	=> $ary['utimestamp'],
				'ITIMESTAMP'	=> $ary['itimestamp'],
			];
		}
		if($this->D['MODUL']['D']['wp_data']['DATA']['D'])
		{
			$k = implode("','",array_keys((array)$this->D['MODUL']['D']['wp_data']['DATA']['D']));
			$qry = $this->C->db()->query("SELECT id, file_id, active, name, utimestamp,itimestamp FROM data_file_seoname WHERE file_id IN ('{$k}') ORDER BY name");
			while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
			{
				$this->D['MODUL']['D']['wp_data']['DATA']['D'][ $ary['file_id'] ]['SEONAME']['D'][ $ary['id'] ] = [
					'ACTIVE'		=> $ary['active'],
					'NAME'			=> $ary['name'],
					'UTIMESTAMP'	=> $ary['utimestamp'],
					'ITIMESTAMP'	=> $ary['itimestamp'],
				];
			}
		}
	}
	
	function set_data($d=null)
	{
		foreach((array)$this->D['MODUL']['D']['wp_data']['DATA']['D'] as $k => $v )
		{
			if($v['ACTIVE'] != -2)
			{
				$IU .= (($IU)?',':'')."('{$k}'";
				$IU .= (isset($v['ACTIVE']))?", '{$v['ACTIVE']}'":", NULL";
				$IU .= (isset($v['EXTENSION']))?", '{$v['EXTENSION']}'":", NULL";
				$IU .= (isset($v['SIZE']))?", '{$v['SIZE']}'":", NULL";
				$IU .= ")";
				
				#SEONAME
				foreach((array)$v['SEONAME']['D'] as $k1 => $v1 )
				{
					if($v1['ACTIVE'] != -2)
					{
						$IU1 .= (($IU1)?',':'')."('{$k1}','{$k}'";
						$IU1 .= (isset($v1['ACTIVE']))?", '{$v1['ACTIVE']}'":", NULL";
						$IU1 .= (isset($v1['NAME']))?", '{$v1['NAME']}'":", NULL";
						$IU1 .= ")";
					}
					else
						$DEL1 .= ($DEL1?',':'')."'{$k1}{$k}'";
				}
			}
			else
				$DEL .= ($DEL?',':'')."'{$k}'";
		}
		
		if($IU)
			$this->C->db()->query("INSERT INTO data_file (id, active, extension, size) VALUES {$IU}
									ON DUPLICATE KEY UPDATE
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE data_file.active END,
									extension = CASE WHEN VALUES(extension) IS NOT NULL THEN VALUES(extension) ELSE data_file.extension END,
									size = CASE WHEN VALUES(size) IS NOT NULL THEN VALUES(size) ELSE data_file.size END
									");
		if($IU1)
			$this->C->db()->query("INSERT INTO data_file_seoname (id, file_id, active, name) VALUES {$IU1}
									ON DUPLICATE KEY UPDATE
									active = CASE WHEN VALUES(active) IS NOT NULL THEN VALUES(active) ELSE data_file_seoname.active END,
									name = CASE WHEN VALUES(name) IS NOT NULL THEN VALUES(name) ELSE data_file_seoname.name END
									");				
		if($DEL)
		{
			$qry = $this->C->db()->query("SELECT id, extension, itimestamp FROM data_file WHERE id IN ({$DEL})");
			while ($ary = $qry->fetch_array(MYSQLI_ASSOC))
			{ #Lösche Dateien
				unlink( "data/wp_data/".date("Y/m", strtotime($ary['itimestamp']))."/{$ary['id']}.{$ary['extension']}" );
				#ToDo: lösche im tmp/data/...
			}
			$this->C->db()->query("DELETE FROM data_file WHERE id IN ({$DEL})");
			$this->C->db()->query("DELETE FROM data_file_seoname WHERE file_id IN ({$DEL})");
			#ToDo: Delete File
		}
		
		if($DEL1)
		{
			$this->C->db()->query("DELETE FROM data_file_seoname WHERE CONCAT(id,file_id) IN ({$DEL1})");
		}
	}
	

}