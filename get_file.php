<?
class wp_data__get_file extends wp_data__get_file__parent
{
	function load($d=null)
	{
		parent::{__function__}();
		$D = &$this->D['MODUL']['D']['wp_data'];
		
		#Beispiel:	data/ID.pdf
		#			data/ID.jpg
		#			data/ID.100x100.jpg
		#Beispiel:	data/SEONAME.pdf
		#			data/SEONAME.jpg
		#			data/SEONAME.100x100.jpg
		$afile = explode('.',$D['GET_FILE']);
		$ext = end($afile);
		
		#$D['DATA']['W']['ID|SEONAME'] = str_replace('.'.$ext,'',$afile[0]); #ToDo Nach ID realisieren
		$D['DATA']['W']['SEONAME'] = $afile[0].'.'.$ext; #xxxx.zz oder xxxx.10x10.zz = ein Seo Name
		$this->C->data()->get_data();
		if(count($this->D['MODUL']['D']['wp_data']['DATA']['D']) == 0)#kein Treffer, dan anhand der ID Prüfen
		{
			$D['DATA']['W'] = null;
			$D['DATA']['W']['ID'] = $afile[0]; #ToDo Nach ID realisieren
			$this->C->data()->get_data();
		}
		if($this->D['MODUL']['D']['wp_data']['DATA']['D'])
			$treffer = array_keys($this->D['MODUL']['D']['wp_data']['DATA']['D']);
		
		switch($ext)
		{
			case 'jpg':
			case 'png':
				

				if($this->D['MODUL']['D']['wp_data']['DATA']['D'])
				{
					$treffer = array_keys($this->D['MODUL']['D']['wp_data']['DATA']['D']);
					$size = explode('x',$afile[1]);
					if(((int)$size[0]) > 0)
					{
						$D['IMAGE'] = [
							'X'				=> $size[0],
							'Y'				=> $size[1],
							'SOURCE_FILE'	=> "data/wp_data/".date("Y/m", strtotime($this->D['MODUL']['D']['wp_data']['DATA']['D'][$treffer[0]]['ITIMESTAMP']))."/{$treffer[0]}.{$ext}",
							'TARGET_DIR'	=> 'tmp/data/',
							'TARGET_FILE'	=> "{$treffer[0]}.{$afile[1]}",#str_replace('.'.$ext,'', $D['GET_FILE']), #ToDo: Speichere file mit ID
							];
						$this->get_image();
						$this->C->library()->CFile()->symlink("tmp/data/{$treffer[0]}.{$afile[1]}.{$ext}", "tmp/data/{$D['GET_FILE']}");
					}
					else
					{
						$P['RETURN']['FILE'] = $D['GET_FILE'];
						$P['SOURCE']['FILE'] = "data/wp_data/".date("Y/m", strtotime($this->D['MODUL']['D']['wp_data']['DATA']['D'][$treffer[0]]['ITIMESTAMP']))."/{$treffer[0]}.{$ext}";
						$this->C->library()->CFile()->symlink($P['SOURCE']['FILE'], "tmp/data/{$P['RETURN']['FILE']}");
						$this->C->library()->CFile()->stream($P);
					}
				}
				break;
			default: #Sonstige dateien
				$P['RETURN']['FILE'] = $D['GET_FILE'];
				$P['SOURCE']['FILE'] = "data/wp_data/".date("Y/m", strtotime($this->D['MODUL']['D']['wp_data']['DATA']['D'][$treffer[0]]['ITIMESTAMP']))."/{$treffer[0]}.{$ext}";
				if(file_exists($P['SOURCE']['FILE']))
				{
					$this->C->library()->CFile()->symlink($P['SOURCE']['FILE'], "tmp/data/{$P['RETURN']['FILE']}");
					$this->C->library()->CFile()->stream($P);
				}
				break;
		}
		#1. get_data()

	}
	
	function get_image()
	{
		parent::{__function__}();
		$D = &$this->D['MODUL']['D']['wp_data'];
		
		$this->C->library()->CFile()->image($D['IMAGE']);
		
		Header ("Content-type: image/jpeg");
		$image_old = @imageCreateFromJPEG($D['IMAGE']['TARGET_DIR'].$D['IMAGE']['TARGET_FILE'].'.jpg');
		if(!$image_old)
		{
			$image_old = ImageCreate (1, 1); 
			$bgc = ImageColorAllocate ($image_old, 255, 255, 255);
		}
		Imagejpeg($image_old);
		imagedestroy($image_old);
	}
}