<form id="form">
	<section class="content">
		<div class="tab-content">
			<table class="table">
				<thead>
					<tr>
						<td><button style="min-width:100px;" class="btn btn-default" onclick="$('#fileinput').trigger('click');" type="button"><i class="fa fa-plus"></i></button></td>
						<td>file</td>
						<td>direct link</td>
						<td>size</td>
						<td>seo link</td>
					</tr>
				</thead>
				<tbody id='filelist'>
				{foreach from=$D.MODUL.D['wp_data'].DATA.D name=DATA item=DATA key=kDATA}
				<tr id="list{$kDATA}">
					<input id="active{$kDATA}" type="hidden" name="D[MODUL][D][wp_data][DATA][D][{$kDATA}][ACTIVE]" value="{$DATA.ACTIVE}">
					<td><button onclick="$('#active{$kDATA}').val('-2');$('#list{$kDATA}').hide();" class="btn btn-default" type="button"><i class="fa fa-trash-o"></i></button></td>
					<td><img src="data/{$kDATA}.20x20.{$DATA.EXTENSION}"></td>
					<td><a href="data/{$kDATA}.{$DATA.EXTENSION}" target='_blank'>{$kDATA|truncate:10:'[...]'}.{$DATA.EXTENSION}</a></td>
					<td>{($DATA.SIZE/1024)|round:1} kb</td>
					<td><button class="button" onclick="create_seo_link('{$kDATA}');" type="button"><i class="fa fa-plus-circle"></i></button>
						<div id='seourllist'>
						{foreach from=$DATA.SEONAME.D name=SEONAME item=SEONAME key=kSEONAME}
							<div id="list{$kDATA}{$kSEONAME}" class="input-group input-group-sm">
								<span class="input-group-btn">
									<input id="active{$kDATA}{$kSEONAME}" type="hidden" name="D[MODUL][D][wp_data][DATA][D][{$kDATA}][SEONAME][D][{$kSEONAME}][ACTIVE]" value="{$SEONAME.ACTIVE}">
									<button onclick="$('#active{$kDATA}{$kSEONAME}').val('-2');$('#list{$kDATA}{$kSEONAME}').hide();" class="btn btn-default" type="button"><i class="fa fa-trash-o"></i></button>
								</span>
								<input name="D[MODUL][D][wp_data][DATA][D][{$kDATA}][SEONAME][D][{$kSEONAME}][NAME]" value="{$SEONAME.NAME}" class="form-control">
								<span class="input-group-addon"><a href="data/{$SEONAME.NAME}" target='_blank'><i class="fa fa-external-link"></i></a></span>
							</div>
						{/foreach}
						</div>
					</td>
				</tr>
				{/foreach}
				</tbody>
			</table>
			<div class="panel-footer text-right">
				<div class="btn-group">
					<button type="button" onclick="$.ajax({ type: 'POST', dataType : 'json', url : '?D[PAGE]=admin__datalist&D[ACTION]=set_data', data : $('#form').serialize() });" class="btn btn-default">Speichern</button>
					<button type="button" onclick="$('#page').load('?D[PAGE]=admin__datalist'); return false;" class="btn btn-default">Abbrechen</button>
				</div>
			</div>
		</div>
	</section>
</form>
<script>
handleFileSelect = function(evt){
	//files = evt.target.files;

	var data = new FormData();
	jQuery.each(jQuery('#fileinput')[0].files, function(i, file) {
		data.append('file['+i+']', file);
	});
	jQuery.ajax({
		url: 'index.php?D[PAGE]=set_file',
		data: data,
		cache: false,
		contentType: false,
		processData: false,
		type: 'POST',
		success: function(data){
			  var dd = JSON.parse(data);
			 for(i=0;i <dd.length;i++)
				create_list(dd[i]);
		}
	});
}
document.getElementById('fileinput').addEventListener('change', handleFileSelect, false);
</script>
<input type="file" name="files[]" id="fileinput"  multiple="multiple" accept="*/*" style="display:none;" />
<script>
	create_list = function(dd)
	{
		$('#filelist').append("<tr>"
		+"<td></td>"
		+"<td></td>"
		+"<td><a href='data/"+dd.URLNAME+"' target='_blank'>"+dd.ID+dd.EXTENSION+"</a></td>"
		+"<td>"+parseInt(dd.SIZE/1024)+" kb</td>"
		+"<td>"+dd.URLNAME+"</td>"
		//+"<input type='hidden' name='D[HELPDESK][D][TICKET][D][{$kTICKET}][MESSAGE][D]["+mid+"][FILE][D]["+id+"][ACTIVE]' value='1'></tr>"
		);
	}
	
	create_seo_link = function(kData)
	{
		id = Date.now();
		$('#seourllist').append(
		"<div id='list"+kData+id+"' class='input-group input-group-sm'>"
		+"	<span class='input-group-btn'>"
		+"		<input id='active"+kData+id+"' type='hidden' name='D[MODUL][D][wp_data][DATA][D]["+kData+"][SEONAME][D]["+id+"][ACTIVE]' value='1'>"
		+"		<button onclick=\"$('#active"+kData+id+"').val('-2');$('#list"+kData+id+"').hide();\" class='btn btn-default' type='button'><i class='fa fa-trash-o'></i></button>"
		+"	</span>"
		+"	<input name='D[MODUL][D][wp_data][DATA][D]["+kData+"][SEONAME][D]["+id+"][NAME]' class='form-control'>"
		+"	<span class='input-group-addon'></span>"
		+"</div>"
		);
	}
</script>