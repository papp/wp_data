<?
class wp_data__20170101000000_install
{
	function __construct(){ global $C, $D; $this->C = &$C; $this->D = &$D; }
	function __call($m, $a){ return $a[0]; } 
	function up()
	{
		$this->C->db()->query("CREATE TABLE `data_file` (
		  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
		  `active` tinyint(1) DEFAULT NULL,
		  `extension` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
		  `size` int(10) UNSIGNED DEFAULT '0',
		  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `utimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");


		$this->C->db()->query("CREATE TABLE `data_file_seoname` (
		  `id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
		  `file_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
		  `active` tinyint(1) DEFAULT NULL,
		  `name` varchar(250) DEFAULT NULL,
		  `itimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `utimestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

		$this->C->db()->query("ALTER TABLE `data_file` ADD PRIMARY KEY (`id`);");
		
		$this->C->db()->query("ALTER TABLE `data_file_seoname` ADD UNIQUE KEY `id` (`id`,`file_id`);");
		return 1;
	}
	
	function down()
	{
		return 1;
	}
}