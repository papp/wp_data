<?
class wp_data__admin__datalist extends wp_data__admin__datalist__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		
		$this->C->user()->check_right(['RIGHT'=>'ADMIN']);
		switch($this->D['ACTION'])
		{
			case 'set_data':
				$this->C->data()->set_data();
				exit;
				break;
			default:
				$this->C->data()->get_data();
				break;
		}
	}
	
	function show($d=null)
	{
		$this->C->library()->smarty()->assign('D', $this->D);
		$this->C->library()->smarty()->display(__dir__.'/tpl/admin__datalist.tpl');
	}
}