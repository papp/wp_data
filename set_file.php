<?
class wp_data__set_file extends wp_data__set_file__parent
{
	function load($d=null)
	{
		parent::{__function__}();
		$D = &$this->D['MODUL']['D']['wp_data'];
		if($_FILES['file'])
		{
			foreach($_FILES['file']['name'] AS $k => $v)
			{
				$this->C->library()->CFile()->copy($_FILES['file'],'tmp');
				$ID = md5_file("tmp/{$v}");
				
				$pathinfo = pathinfo($v);
				#ToDo: Prüfen ob die ID bereits in der DB exsistiert und dann nur überspringen.
				$this->C->library()->CFile()->move("tmp/{$v}","data/wp_data/".date("Y/m")."/{$ID}.{$pathinfo['extension']}");
				$SIZE = filesize("data/wp_data/".date("Y/m")."/{$ID}.{$pathinfo['extension']}");
				#$pathinfo = pathinfo("data/wp_data/".date("Y/m")."/{$ID}.{$pathinfo['extension']}");
				$this->D['MODUL']['D']['wp_data']['DATA']['D'][$ID] = [
					'ACTIVE'	=> 1,
					#'NAME'		=> $v,
					'EXTENSION'	=> $pathinfo['extension'],
					'SIZE'		=> $SIZE,
				];
				
				$SEO_NAME = time().'_'.$v;
				$this->D['MODUL']['D']['wp_data']['DATA']['D'][$ID]['SEONAME']['D'][md5($SEO_NAME)] = [
					'ACTIVE'	=> 1,
					'NAME'		=> $SEO_NAME,
				];
				
				$js_return[] = [
					'ID'		=> $ID,
					'SEO_ID'	=> md5($SEO_NAME),
					'ACTIVE'	=> 1,
					'URLNAME'	=> $SEO_NAME,
					'NAME'		=> $v,
					'EXTENSION'	=> $pathinfo['extension'],
					'SIZE'		=> $SIZE,
				];
				
			}
			$this->C->data()->set_data();
			exit(json_encode($js_return));
		}
	}
}